﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Kakao2017 {
    public class Program {
        public static void Main() {
            Camping p = new Camping();

            Stopwatch stopwatch = new Stopwatch(); //객체 선언
            stopwatch.Start(); // 시간측정 시작
            int sol=p.TestSolution(5000);

            stopwatch.Stop(); // 시간측정 끝
            Console.WriteLine($"정답:{sol} 시간:{stopwatch.ElapsedMilliseconds}");
        }

    }
}
