﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kakao2017 {
    /// <summary>
    /// 카카오2017 예선 컬러링북
    /// https://programmers.co.kr/learn/courses/30/lessons/1829
    /// </summary>
    public class ColoringBook {
        public static List<Cell> map = new List<Cell>();
        public static List<Area> areas=new List<Area>();

        public struct Cell {
            public int[] pos;
            public int color;
        }

        public class Area {
            public int color;
            public List<int[]> posList = new List<int[]>();
            public List<int[]> isChecked = new List<int[]>();

            public bool Exists(int[] val) {
                return posList.Exists(x => x[0] == val[0] && x[1] == val[1]);
            }
            public bool IsCheck(int[] val) {
                return isChecked.Exists(x => x[0] == val[0] && x[1] == val[1]);
            }
            public void AddCell(int m,int n,Cell cell) {
                if (IsCheck(cell.pos)) return;
                isChecked.Add(cell.pos);
                if (cell.color == 0) return;
                if (cell.color != color) return;
                Console.WriteLine($"Area에 Cell 추가 (Cell:{cell.pos[0]}{cell.pos[1]})");
                posList.Add(cell.pos);
                var others = FindOthers(m, n, cell);
                foreach (var it in others) {
                    AddCell(m, n, it);
                }
            }
        }

        

        public int[] Solution(int m, int n, int[][] picture) {
            //Map 세팅
            Console.WriteLine("==맵세팅 시작==");
            map.Clear();
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    Cell cell = new Cell();
                    cell.pos = new int[] { i, j };
                    cell.color = picture[i][j];
                    map.Add(cell);
                    Console.WriteLine($"Pos:{cell.pos[0]}{cell.pos[1]} , Color:{cell.color}");
                }
            }
            Console.WriteLine("==맵세팅 끝==");
            //Areas 세팅
            Console.WriteLine("==Area 세팅 시작==");
            areas.Clear();
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    var cell=map.Find(x => x.pos[0] == i && x.pos[1] == j);
                    if (cell.color == 0) continue;
                    if (areas.Exists(x => x.Exists(cell.pos))) continue;
                    Console.WriteLine($"Area최초 생성 (Color:{cell.color},Cell:{cell.pos[0]}{cell.pos[1]})");
                    Area area = new Area();
                    area.color = cell.color;
                    area.AddCell(m,n,cell);
                    areas.Add(area);
                }
            }
            Console.WriteLine("==Area 세팅 끝==");



            int numberOfArea = areas.Count;
            int maxSizeOfOneArea = areas.Max(x => x.posList.Count);



            int[] answer = new int[2];
            answer[0] = numberOfArea;
            answer[1] = maxSizeOfOneArea;
            return answer;
        }


        public static List<Cell> FindOthers(int m,int n,Cell cell) {
            var others = new List<Cell>();
            int[] pos = cell.pos;

            if (pos[0] > 0) {
                var left = map.Find(x => x.pos[0] == pos[0] - 1 && x.pos[1]== pos[1]);
                others.Add(left);
            }
            if (pos[0] < m-1) {
                var right = map.Find(x => x.pos[0] == pos[0] + 1 && x.pos[1] == pos[1]);
                others.Add(right);
            }
            if (pos[1] > 0) {
                var up = map.Find(x => x.pos[0] == pos[0]  && x.pos[1] ==pos[1]- 1);
                others.Add(up);
            }
            if (pos[1] < n - 1) {
                var down = map.Find(x => x.pos[0] == pos[0] && x.pos[1] == pos[1] + 1);
                others.Add(down);
            }
            return others;
        }

        private void TestCase() {
            //Case1 (4,5)
            //int m = 6;
            //int n = 4;
            //int[][] picture = new int[][] {
            //    new int[] { 1, 1, 1, 0 },
            //    new int[] { 1, 2, 2, 0  },
            //    new int[] { 1, 0, 0, 1 },
            //    new int[] { 0, 0, 0, 1 },
            //    new int[] { 0, 0, 0, 3 },
            //    new int[] { 0, 0, 0, 3 }
            //};

            //Case2 (2,6)
            //int m = 6;
            //int n = 4;
            //int[][] picture = new int[][] {
            //    new int[] { 1, 1, 1, 0 },
            //    new int[] { 1, 1, 1, 0 },
            //    new int[] { 0, 0, 0, 1 },
            //    new int[] { 0, 0, 0, 1 },
            //    new int[] { 0, 0, 0, 1 },
            //    new int[] { 0, 0, 0, 1 }
            //};

            //Case2(3, 5)
            //int m = 3;
            //int n = 5;
            //int[][] picture = new int[][] {
            //    new int[] { 1, 0, 0, 0, 1 },
            //    new int[] { 0, 1, 2, 1, 0 },
            //    new int[] { 0, 1, 1, 1, 0 }
            //};

            //Case3(3, 2)
            //int m = 3;
            //int n = 2;
            //int[][] picture = new int[][] {
            //    new int[] { 1, 0},
            //    new int[] { 0, 2 },
            //    new int[] { 127, 2 },
            //};

            //Case4(6, 23)
            int m = 7;
            int n = 6;
            int[][] picture = new int[][] {
                new int[] { 0,1,1,1,1,2},
                new int[] { 1,1,0,3,0,2 },
                new int[] { 1,1,1,1,1,2 },
                new int[] { 5,1,3,3,0,2 },
                new int[] { 1,1,1,1,1,2 },
                new int[] { 1,1,0,3,0,2 },
                new int[] { 0,1,1,1,1,2 },
            };

            int[] sol = Solution(m, n, picture);
            Console.WriteLine("정답: " + sol[0] + "," + sol[1]);
        }
    }
}
