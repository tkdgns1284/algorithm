﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kakao2017 {
    /// <summary>
    /// 카카오2017 예선 보행자천국
    /// https://programmers.co.kr/learn/courses/30/lessons/1832
    /// </summary>
    public class Heaven {
        public const int MOD = 20170805;
        public static int m;
        public static int n;
        public static int[,] cityMap;

        public static int[] START_POS = { 0, 0 };
        public static int[] EXIT_POS = new int[2];
        
        //현재위치
        public static int[] curPos = { 0, 0 };


        //정답개수
        public static int solCount;

        public int Solution(int m, int n, int[,] cityMap) {
            Heaven.m = m;
            Heaven.n = n;
            Heaven.cityMap = cityMap;
            EXIT_POS[0] = m - 1;
            EXIT_POS[1] = n - 1;
            if (cityMap[m - 1, n - 1] != 0) {
                Console.WriteLine("[CityMap 값 오류]");
                return -10000;
            }
            return FindExit(EXIT_POS,0) % 20170805;
        }

        //dir : 0-둘다 ,1-가로, 2-세로
        private int FindExit(int[] targetPos,int dir) {
            if (Enumerable.SequenceEqual(targetPos, START_POS)) return 1;
            if (targetPos[0] < 0) return 0;
            if (targetPos[1] < 0) return 0;
            int cityState = cityMap[targetPos[0], targetPos[1]];
            if (cityState == 1) return 0;
            int[] leftPos = { targetPos[0] - 1, targetPos[1] };
            int[] upPos = { targetPos[0], targetPos[1] - 1 };

            if (cityState == 0) {
                return FindExit(leftPos, 1) + FindExit(upPos, 2);
            }
            else {
                if (dir == 1) {
                    return FindExit(leftPos, 1);
                } else if (dir == 2) {
                    return FindExit(upPos, 1);
                }
                return -100000;
            } 
        }

    }

    
}
