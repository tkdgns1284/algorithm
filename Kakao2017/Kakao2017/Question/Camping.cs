﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kakao2017 {
    /// <summary>
    /// 카카오2017 예선 캠핑
    /// https://programmers.co.kr/learn/courses/30/lessons/1833
    /// Todo: 성능개선 필요
    /// </summary>
    public class Camping {
        public static int n;
        public static int[][] data;
        

        public int Solution(int n, int[][] data) {
            int answer = 0;
            Camping.n = n;
            Camping.data = data;
            for (int i = 0; i < n-1; i++) {
                for (int j = i + 1; j < n; j++) {
                    if (IsEnable(data[i], data[j])) {
                        //Console.WriteLine($"------------------[{DebugString(data[i])},{DebugString(data[j])}]는 설치가능----------------");
                        answer += 1;
                    } 
                }
            }

            return answer;
        }
        public int TestSolution(int count) {
            int testCount = count;
            int[][] data = new int[testCount][];
            var r = new Random();
            for (int i = 0; i < testCount; i++) {
                data[i] = new int[2];
                while (true) {
                    data[i][0] = r.Next();
                    data[i][1] = r.Next();

                    break;
                }
            }
            Console.WriteLine("세팅완료");
            return Solution(testCount, data);
        }

        private bool IsEnable(int[] stick1, int[] stick2) {
            if (stick1[0] == stick2[0] || stick1[1] == stick2[1]) {
                //Console.WriteLine($"[{DebugString(stick1)},{DebugString(stick2)}]는 거른다. (크기가 0)");
                return false;
            }

            int minX = Math.Min(stick1[0], stick2[0]);
            int maxX = Math.Max(stick1[0], stick2[0]);
            int minY = Math.Min(stick1[1], stick2[1]);
            int maxY = Math.Max(stick1[1], stick2[1]);


            bool isExists = Array.Exists(data, otherStick => {
                return (minX < otherStick[0] && otherStick[0] < maxX) && (minY < otherStick[1] && otherStick[1] < maxY);
            });

            if (isExists) {
                //Console.WriteLine($"[{DebugString(stick1)},{DebugString(stick2)}]는 거른다. (안에 쐐기)");
            }

            return !isExists;
        }


        private string DebugString(int[] array) {
            return $"({array[0]},{array[1]})";
        }
    }
}
